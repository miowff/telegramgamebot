﻿namespace TelegramGameBot.Core.Entities
{
    public sealed class Player : BaseEntity
    {
        public string? Name { get; set; }
        public string? ChatId { get; set; }
        public int GamesPlayed { get; set; }
        public int Wins { get; set; }
        public Player()
        {
            this.Wins = 0;
            this.GamesPlayed = 0;
        }
    }
}
