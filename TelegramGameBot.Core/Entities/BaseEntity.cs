﻿
namespace TelegramGameBot.Core.Entities
{
    public class BaseEntity
    {
        public string Id { get; set; }
        public DateTime CreationTime { get; set; }
        public BaseEntity()
        {
            this.Id = Guid.NewGuid().ToString();
            this.CreationTime = DateTime.Now;
        }
    }
}
