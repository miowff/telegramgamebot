using Microsoft.EntityFrameworkCore;
using TelegramGameBot.Data;
using TelegramGameBot.Domain;
using TelegramGameBot.Domain.Services;
using TelegramGameBot.Domain.ServicesInterfaces;
using TelegramGameBot.Domain.TelegramBotMessages;
var builder = WebApplication.CreateBuilder(args);
// Add services to the container.
builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("TelegramGameBot"));
});
builder.Services.AddControllers().AddNewtonsoftJson();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<TelegramBot,TelegramBot>();
builder.Services.AddScoped<IPlayersRepository, PlayersRepiository>();
builder.Services.AddTransient<ICommandParser,CommandParser>();
builder.Services.AddScoped<IUpdateHandler, UpdateHandler>();
builder.Services.AddTransient<IGame, GameService>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});
app.Run();
