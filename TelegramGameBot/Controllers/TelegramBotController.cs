﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramGameBot.Core.Entities;
using TelegramGameBot.Data;
using TelegramGameBot.Domain;
using TelegramGameBot.Domain.Commands;
using TelegramGameBot.Domain.ServicesInterfaces;
namespace TelegramGameBot.Controllers
{
    [ApiController]
    [Route("api/message")]
    public class TelegramBotController : ControllerBase
    {
        private readonly IUpdateHandler _updateHandler;
        public TelegramBotController(IUpdateHandler updateHandler)
        {
            this._updateHandler = updateHandler;
        }
        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] object update)
        {
            try
            {
                var upd = JsonConvert.DeserializeObject<Update>(update.ToString());
                if(upd == null)
                {
                    return BadRequest();
                }
                var chat = upd.Message?.Chat;
                if (chat == null)
                {
                    return BadRequest();
                }
                await _updateHandler.HandleUpdate(upd);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
