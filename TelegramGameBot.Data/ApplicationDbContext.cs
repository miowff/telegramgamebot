﻿using Microsoft.EntityFrameworkCore;
using TelegramGameBot.Core.Entities;

namespace TelegramGameBot.Data
{
    public sealed class ApplicationDbContext : DbContext
    {
        public DbSet<Player> Players { get; set; } = null!;
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
