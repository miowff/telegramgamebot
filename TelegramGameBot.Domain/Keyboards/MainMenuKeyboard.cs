﻿using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramGameBot.Domain.Keyboards
{
    public static class MainMenuKeyboard
    {
        public static ReplyKeyboardMarkup GetMainMenuKeyboard()
        {
            ReplyKeyboardMarkup keyboard = new(new[]
                    {
                        new KeyboardButton[]{"StartGame" },
                        new KeyboardButton[]{"MyProfile" }
                    });
            return keyboard;
        }
    }
}
