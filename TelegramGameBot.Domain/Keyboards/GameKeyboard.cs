﻿
using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramGameBot.Domain.Keyboards
{
    public static class GameKeyboard
    {
        public static ReplyKeyboardMarkup GetGameKeyboard()
        {
            ReplyKeyboardMarkup keyboard = new(new[]
                    {
                        new KeyboardButton[]{"ChoseRock" },
                        new KeyboardButton[]{"ChosePaper" },
                        new KeyboardButton[]{"ChoseScissors" },
                        new KeyboardButton[]{"Surrender" },
                    });
            return keyboard;
        }
    }
}
