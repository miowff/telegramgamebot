﻿namespace TelegramGameBot.Domain.TelegramBotMessages
{
    public class BotMessages
    {
        public const string GameStartedAlert = "Game is started!Good luck!";
        public const string IncorrectCommandAlert = "Wrong command";
        public const string Surrendermessage = "You are surrenderred!";
        public const string NoMoreRocksInDeckAlert = "No more rocks in your deck";
        public const string NoMorePaperInDeckAlert = "No more paper in your deck";
        public const string NoMoreScissorsInDeckAlert = "No more scissors in your deck";
        public const string WonAlert = "You won! Game is over!";
        public const string LoseAlert = "Well played, better luck next time!";
        public static string CreateUserInfoMessage(int gamesPlayed, int wins, string name)
        {
            return $"👤{name}👤\nGames played:{gamesPlayed}\nWins:{wins}";
        }
    }
}
