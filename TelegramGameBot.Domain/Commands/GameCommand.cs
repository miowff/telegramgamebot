﻿namespace TelegramGameBot.Domain.Commands
{
    public  enum GameCommand
    {
        ChoseRock,
        ChosePaper,
        ChoseScissors,
        Surrender,
        IncorrectCommand
    }
}
