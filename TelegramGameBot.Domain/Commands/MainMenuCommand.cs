﻿namespace TelegramGameBot.Domain.Commands
{
    public enum MainMenuCommand
    {
        StartGame,
        MyProfile,
        IncorrectCommand
    }
}
