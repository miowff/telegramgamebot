﻿namespace TelegramGameBot.Domain.Dtos
{
    public sealed class GameResultDto
    {
        public GameData gameData { get; set; }
        public string message { get; set; }
        public GameResultDto(GameData data, string resultMessage)
        {
            this.gameData = data;
            this.message = resultMessage;
        }
    }
}
