﻿using System.Text;
namespace TelegramGameBot.Domain
{
    public sealed class GameData
    {
        public State state { get; set; }
        public List<string> playerDeck;
        public List<string> botDeck;
        public int botScore { get; set; }
        public int userScore { get; set; }
        public GameData()
        {
            this.state = State.Inactive;
            this.playerDeck = new List<string>()
            {
            "rock","rock","rock",
            "paper","paper","paper",
            "scissors","scissors","scissors"
            };
            this.botDeck = new List<string>()
            {
            "rock","rock","rock",
            "paper","paper","paper",
            "scissors","scissors","scissors"
            };
        }
        public void SetState(State state)
        {
            this.state = state;
        }
        public bool IsPossibleToGetRock()
        {
            if (playerDeck.Count(x => x == "rock") > 0)
            {
                return true;
            }
            return false;
        }
        public bool IsAbleToTakePaper()
        {
            if (playerDeck.Count(x => x == "paper") > 0)
            {
                return true;
            }
            return false;
        }
        public bool IsAbleToTakeScissors()
        {
            if (playerDeck.Count(x => x == "scissors") > 0)
            {
                return true;
            }
            return false;
        }
        public string GetPlayerDeckInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Your deck:");
            sb.Append("👊:");
            sb.Append(this.playerDeck.Count(x => x == "rock"));
            sb.Append("✌️:");
            sb.Append(this.playerDeck.Count(x => x == "scissors"));
            sb.Append("✋");
            sb.Append(this.playerDeck.Count(x => x == "paper"));
            return sb.ToString();
        }
        public string GatBotDeckInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Bot deck:");
            sb.Append("👊:");
            sb.Append(this.botDeck.Count(x => x == "rock"));
            sb.Append("✌️:");
            sb.Append(this.botDeck.Count(x => x == "scissors"));
            sb.Append("✋");
            sb.Append(this.botDeck.Count(x => x == "paper"));
            return sb.ToString();
        }
    }
}
