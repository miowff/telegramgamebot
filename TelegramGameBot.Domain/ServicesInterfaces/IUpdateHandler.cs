﻿using Telegram.Bot.Types;
namespace TelegramGameBot.Domain.ServicesInterfaces
{
    public interface IUpdateHandler
    {
        public Task HandleUpdate(Update update);
    }
}
