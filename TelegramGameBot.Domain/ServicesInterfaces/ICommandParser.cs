﻿using TelegramGameBot.Domain.Commands;
namespace TelegramGameBot.Domain.ServicesInterfaces
{
    public interface ICommandParser
    {
        MainMenuCommand ParseMainMenuCommand(string command);
        GameCommand ParseGameCommand(string command);
    }
}
