﻿using TelegramGameBot.Domain.Commands;
using TelegramGameBot.Domain.Dtos;
namespace TelegramGameBot.Domain.ServicesInterfaces
{
    public interface IGame
    {
        GameResultDto PlayRound(GameCommand command,GameData gameData);
        bool IsGameCanBeContinued(GameData gameData);
        bool IsPlayerWon(GameData gameData);
    }
}
