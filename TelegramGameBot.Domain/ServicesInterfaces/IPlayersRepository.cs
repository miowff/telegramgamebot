﻿using TelegramGameBot.Core.Entities;
namespace TelegramGameBot.Domain.ServicesInterfaces
{
    public interface IPlayersRepository
    {
        Task<Player> GetByChatId(string chatId);
        Task<bool> Delete(string chatId);
        Task<bool> AddNew(Player player);
        Task<bool> Update(Player player);
    }
}
