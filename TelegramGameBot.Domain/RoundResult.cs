﻿namespace TelegramGameBot.Domain
{
    public enum RoundResult
    {
        Won,
        Lose,
        Tie
    }
}
