﻿using TelegramGameBot.Domain.Commands;
using TelegramGameBot.Domain.ServicesInterfaces;
namespace TelegramGameBot.Domain.Services
{
    public sealed class CommandParser : ICommandParser
    {
        public GameCommand ParseGameCommand(string command)
        {
            if(Enum.TryParse(command,true,out GameCommand gameCommand))
            {
                return gameCommand;
            }
            else
            {
                return GameCommand.IncorrectCommand;
            }
        }
        public MainMenuCommand ParseMainMenuCommand(string command)
        {
            if(Enum.TryParse(command,true,out MainMenuCommand mainMenuCommand))
            {
                return mainMenuCommand;
            }
            else
            {
                return MainMenuCommand.IncorrectCommand;
            }
        }
    }
}
