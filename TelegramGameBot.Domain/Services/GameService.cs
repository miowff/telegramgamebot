﻿using TelegramGameBot.Domain.Commands;
using TelegramGameBot.Domain.Dtos;
using TelegramGameBot.Domain.ServicesInterfaces;
namespace TelegramGameBot.Domain.Services
{
    public sealed class GameService : IGame
    {
        private int rockValue;
        private int paperValue;
        private int scissorsValue;
        public GameResultDto PlayRound(GameCommand command, GameData gameData)
        {
            var botCard = BotTurn(gameData);
            switch (command)
            {
                case GameCommand.ChoseRock:
                    var roundResult = GetRoundResult(botCard, "rock");
                    gameData.playerDeck.Remove("rock");
                    gameData.botDeck.Remove(botCard);
                    return CreateResult(roundResult, gameData);
                case GameCommand.ChosePaper:
                    roundResult = GetRoundResult(botCard, "paper");
                    gameData.playerDeck.Remove("paper");
                    gameData.botDeck.Remove(botCard);
                    return CreateResult(roundResult, gameData);
                case GameCommand.ChoseScissors:
                    roundResult = GetRoundResult(botCard, "scissors");
                    gameData.playerDeck.Remove("scissors");
                    gameData.botDeck.Remove(botCard);
                    return CreateResult(roundResult, gameData);
                default:
                    throw new NotImplementedException();
            }
        }
        public bool IsPlayerWon(GameData gameData)
        {
            if (gameData.userScore > gameData.botScore)
            {
                return true;
            }
            return false;
        }
        public bool IsGameCanBeContinued(GameData gameData)
        {
            if (gameData.playerDeck.Count == 0 || gameData.botDeck.Count == 0)
            {
                return false;
            }
            return true;
        }
        private GameResultDto CreateResult(RoundResult roundResult, GameData gameData)
        {
            switch (roundResult)
            {
                case RoundResult.Won:
                    gameData.userScore++;
                    return new GameResultDto(gameData, $"✅You won round✅\n\tScore\nYou👤:{gameData.userScore}|Bot🤖:{gameData.botScore}\n{gameData.GetPlayerDeckInfo()}\n{gameData.GatBotDeckInfo()}");
                case RoundResult.Lose:
                    gameData.botScore++;
                    return new GameResultDto(gameData, $"❌You lose round❌\t\nScore\n You👤:{gameData.userScore}|Bot🤖:{gameData.botScore}\n{gameData.GetPlayerDeckInfo()}\n{gameData.GatBotDeckInfo()}");
                case RoundResult.Tie:
                    return new GameResultDto(gameData, $"❓Thre is tie❓\t\nScore\nYou👤:{gameData.userScore}|Bot🤖:{gameData.botScore}\n{gameData.GetPlayerDeckInfo()}\n{gameData.GatBotDeckInfo()}");
                default:
                    return new GameResultDto(gameData, $"\tScore\nYou👤:{gameData.userScore}|Bot🤖:{gameData.botScore}\n{gameData.GetPlayerDeckInfo()}\n{gameData.GatBotDeckInfo()}");
            }

        }
        private string BotTurn(GameData gameData)
        {
            var rand = new Random();
            var botCard = gameData.botDeck[rand.Next(0, gameData.botDeck.Count)];
            return botCard;
        }
        private RoundResult GetRoundResult(string botCard, string playerCard)
        {
            SetValues(botCard);
            var result = GetValue(playerCard);
            if (result == 1)
            {
                return RoundResult.Won;
            }
            if (result == 0)
            {
                return RoundResult.Tie;
            }
            return RoundResult.Lose;
        }
        private int GetValue(string playerCard)
        {
            if (playerCard == "rock")
            {
                return rockValue;
            }
            if (playerCard == "paper")
            {
                return paperValue;
            }
            return scissorsValue;
        }
        private void SetValues(string botCard)
        {
            switch (botCard)
            {
                case "rock":
                    this.rockValue = 0;
                    this.paperValue = 1;
                    this.scissorsValue = -1;
                    break;
                case "paper":
                    this.paperValue = 0;
                    this.rockValue = -1;
                    this.scissorsValue = 1;
                    break;
                case "scissors":
                    this.scissorsValue = 0;
                    this.paperValue = -1;
                    this.rockValue = 1;
                    break;
            }
        }
    }
}
