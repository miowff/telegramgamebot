﻿using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramGameBot.Core.Entities;
using TelegramGameBot.Domain.Commands;
using TelegramGameBot.Domain.Keyboards;
using TelegramGameBot.Domain.ServicesInterfaces;
using TelegramGameBot.Domain.TelegramBotMessages;
namespace TelegramGameBot.Domain.Services
{
    public sealed class UpdateHandler : IUpdateHandler
    {
        private static readonly Dictionary<string, GameData> _gameData = new Dictionary<string, GameData>();
        private readonly ICommandParser _commandParser;
        private readonly TelegramBotClient _botClient;
        private readonly IGame _gameService;
        private readonly IPlayersRepository _playersRepository;
        public UpdateHandler(ICommandParser commandParser, TelegramBot bot, IGame gameService, IPlayersRepository playersRepository)
        {
            this._playersRepository = playersRepository;
            this._gameService = gameService;
            this._botClient = bot.GetBot().Result;
            this._commandParser = commandParser;
        }

        public async Task HandleUpdate(Update update)
        {
            var existedPlayer = await _playersRepository.GetByChatId(update.Message.Chat.Id.ToString());
            if (existedPlayer == null)
            {
                var newPlayer = new Player()
                {
                    ChatId = update.Message.Chat.Id.ToString(),
                    Name = update.Message.Chat.Username
                };
                await _playersRepository.AddNew(newPlayer);
            }
            var gameData = _gameData.FirstOrDefault(entry => entry.Key == update.Message.Chat.Id.ToString()).Value;
            if (gameData == null)
            {
                _gameData.Add(update.Message.Chat.Id.ToString(), new GameData());
                gameData = _gameData[update.Message.Chat.Id.ToString()];
            }
            switch (gameData.state)
            {
                case State.Inactive:
                    await HandleMainMenuCommand(update, gameData);
                    break;
                case State.InGame:
                    await HandleGameCommand(update, gameData);
                    break;
            }
        }
        private async Task HandleMainMenuCommand(Update update, GameData gameData)
        {

            var menuCommand = _commandParser.ParseMainMenuCommand(update.Message.Text);
            switch (menuCommand)
            {
                case MainMenuCommand.StartGame:
                    await _botClient.SendTextMessageAsync(update.Message.Chat.Id, BotMessages.GameStartedAlert,
                        replyMarkup: GameKeyboard.GetGameKeyboard());
                    gameData.SetState(State.InGame);
                    break;
                case MainMenuCommand.MyProfile:
                    var existedUser = await _playersRepository.GetByChatId(update.Message.Chat.Id.ToString());
                    await _botClient.SendTextMessageAsync(update.Message.Chat.Id, BotMessages.CreateUserInfoMessage(existedUser.GamesPlayed, existedUser.Wins, existedUser.Name),
                        replyMarkup: MainMenuKeyboard.GetMainMenuKeyboard());
                    break;
                default:
                    await _botClient.SendTextMessageAsync(update.Message.Chat.Id,BotMessages.IncorrectCommandAlert,
                        replyMarkup: MainMenuKeyboard.GetMainMenuKeyboard());
                    break;
            }
        }
        private async Task HandleGameCommand(Update update, GameData gameData)
        {
            var gameCommand = _commandParser.ParseGameCommand(update.Message.Text);
            switch (gameCommand)
            {
                case GameCommand.Surrender:
                    await _botClient.SendTextMessageAsync(update.Message.Chat.Id, BotMessages.Surrendermessage,
                        replyMarkup: MainMenuKeyboard.GetMainMenuKeyboard());
                    _gameData[update.Message.Chat.Id.ToString()] = new GameData();
                    break;
                case GameCommand.ChoseRock:
                    if (!gameData.IsPossibleToGetRock())
                    {
                        await _botClient.SendTextMessageAsync(update.Message.Chat.Id,
                            BotMessages.NoMoreRocksInDeckAlert,
                            replyMarkup: GameKeyboard.GetGameKeyboard());
                        break;
                    }
                    await _botClient.SendTextMessageAsync(update.Message.Chat.Id,
                        _gameService.PlayRound(GameCommand.ChoseRock, gameData).message,
                        replyMarkup: GameKeyboard.GetGameKeyboard());
                    break;
                case GameCommand.ChosePaper:
                    if(!gameData.IsAbleToTakePaper())
                    {
                        await _botClient.SendTextMessageAsync(update.Message.Chat.Id,
                            BotMessages.NoMorePaperInDeckAlert,
                            replyMarkup: GameKeyboard.GetGameKeyboard());
                        break;
                    }
                    await _botClient.SendTextMessageAsync(update.Message.Chat.Id,
                        _gameService.PlayRound(GameCommand.ChosePaper, gameData).message,
                        replyMarkup: GameKeyboard.GetGameKeyboard());
                    break;
                case GameCommand.ChoseScissors:
                    if (!gameData.IsAbleToTakeScissors())
                    {
                        await _botClient.SendTextMessageAsync(update.Message.Chat.Id,
                            BotMessages.NoMoreScissorsInDeckAlert,
                            replyMarkup: GameKeyboard.GetGameKeyboard());
                        break;
                    }
                    await _botClient.SendTextMessageAsync(update.Message.Chat.Id,
                        _gameService.PlayRound(GameCommand.ChoseScissors, gameData).message,
                        replyMarkup: GameKeyboard.GetGameKeyboard());
                    break;
                default:
                    await _botClient.SendTextMessageAsync(update.Message.Chat.Id, BotMessages.IncorrectCommandAlert,
                        replyMarkup: GameKeyboard.GetGameKeyboard());
                    break;
            }
            if (!_gameService.IsGameCanBeContinued(gameData))
            {
                var existedUser = await _playersRepository.GetByChatId(update.Message.Chat.Id.ToString());
                existedUser.GamesPlayed++;
                if (gameData.userScore > gameData.botScore)
                {
                    existedUser.Wins++;
                    await _botClient.SendTextMessageAsync(update.Message.Chat.Id, BotMessages.WonAlert,
                        replyMarkup: MainMenuKeyboard.GetMainMenuKeyboard());
                    _gameData[update.Message.Chat.Id.ToString()] = new GameData();
                }
                else
                {
                    await _botClient.SendTextMessageAsync(update.Message.Chat.Id,BotMessages.LoseAlert,
                        replyMarkup: MainMenuKeyboard.GetMainMenuKeyboard());
                    _gameData[update.Message.Chat.Id.ToString()] = new GameData();
                }
                await _playersRepository.Update(existedUser);
            }
        }
    }
}
