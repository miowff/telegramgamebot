﻿using Microsoft.EntityFrameworkCore;
using TelegramGameBot.Core.Entities;
using TelegramGameBot.Data;
using TelegramGameBot.Domain.ServicesInterfaces;
namespace TelegramGameBot.Domain.Services
{
    public sealed class PlayersRepiository : IPlayersRepository
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly DbSet<Player> _dbSet;
        public PlayersRepiository(ApplicationDbContext context)
        {
            this._dbContext = context;
            this._dbSet=_dbContext.Set<Player>();
        }

        public async Task<bool> AddNew(Player player)
        {
            await _dbSet.AddAsync(player);
            await _dbContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Delete(string chatId)
        {
            var existedPlayer = await _dbSet.FirstOrDefaultAsync(player => player.ChatId == chatId);
            if(existedPlayer == null)
            {
                return false;
            }
            _dbContext.Remove(existedPlayer);
            await _dbContext.SaveChangesAsync();
            return true;
        }

        public async Task<Player> GetByChatId(string chatId)
        {
            var existedPlayer = await _dbSet.FirstOrDefaultAsync(player => player.ChatId == chatId);
            return existedPlayer;
        }

        public async Task<bool> Update(Player player)
        {
            var existedEntity = await _dbSet.FirstOrDefaultAsync(p => p.ChatId == player.ChatId);
            if( existedEntity == null)
            {
                return false;
            }
            _dbSet.Update(player);
            await _dbContext.SaveChangesAsync();
            return true;
        }
    }
}
